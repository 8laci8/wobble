:- module(server,[main/1]).

:- use_module(wobble).

main([]):-
	write("\n Write a . and press Enter to stop!\n\n"),
	server_start,!.
main(_).