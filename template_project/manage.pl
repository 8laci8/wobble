:- module( manage, []).

:- use_module("../src/core").

:- initialization main(config).

main(Config):- run_server(Config),halt.
main(_):- halt(1).