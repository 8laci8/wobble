:- module(routes,[]).

:- use_module(library(http/http_dispatch)).
:- [controllers/controllers].

:- http_handler(/, say_hi, []).