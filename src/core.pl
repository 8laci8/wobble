:- module(core,[run_server/1]).

:- use_module(utils).
:- use_module(library(http/thread_httpd)).
%:- use_module(library(http/http_dispatch)).

:- on_signal(int, _, stop).

stop(_Signal):- thread_send_message(main, stop), nl.

run_server(Config):- setup(Config), thread_get_message(stop).

setup(Config):-
	use_module(Config),
	%load(urls),
	port(Port),
	http_server(dispatcher, [port(Port)]),
	print_message(informational, wobble_exit_command).

%load(Module):- 
%	functor(Holder,Module,1),
%	current_predicate(_,Holder),
%	call(Module,Path),
%	use_module(Path),!.
%load(Module):- use_module(Module).

%loadall([]).
%loadall([Module|Ms]):-use_module(Module),loadall(Ms).

dispatcher(Request):-
	lang('en-US'),type('text/plain'),status('201'),
	nl,
	memberchk(path(X),Request),split_string(X,['/'],[],Y),
	format('Hello World!~n~w~n',[Y]).

header(T,C):- format('~w: ~w~N', [T,C]).

lang(L):- header('Content-Language',L).
type(T):- header('Content-Type', T).
status(S):- header('Status',S).

:- multifile prolog:message/3.

prolog:message(wobble_exit_command) -->
	[ "Press CTRL+C to stop the server." ].