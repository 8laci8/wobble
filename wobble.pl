:- module(wobble,[server_start/0]).

:- use_module(config).
:- use_module(app/routes).

:- use_module(library(http/thread_httpd)).
:- use_module(library(http/http_dispatch)).
:- use_module(library(http/http_log)).
:- use_module(library(st/st_render)).

server_setup:-
		server_port(Port),
        http_server(http_dispatch, [port(Port)]).

server_start:-
	server_setup,
	stay.

console_log(Log):- console_log(Log,[]).
console_log(Log,Args):- atom_concat(Log,'\n',Logln),format(user_output,Logln,Args).

stay:-skip("."),halt.
stay:-read(_),stay.

render_site(Wiev,Content):-
		format('Content-type: text/html~n~n'),
       	current_output(Out),
       	atom_concat("app/views/",Wiev,Path),
       	wobble:st_render_file(Path, Content , Out, [extension('html.epl'),cache(true)]).


