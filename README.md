# Wobble

A full stack web framework in (SWI-)Prolog!

# How to?

Make sure you have [SWI-Prolog](http://www.swi-prolog.org/) installed and you downloaded the [Simple-template](https://github.com/rla/simple-template) engine,
After downloading go to the main directory and use:

> $ swipl -g main -s src/wobble.pl

to start your server.

To stop it type a "." and press enter!

# Technologies:

- This project mostly relies on [SWI-Prolog's HTTP Support package](http://www.swi-prolog.org/pldoc/doc_for?object=section(%27packages/http.html%27))
- Other than that I use [Simple-template](https://github.com/rla/simple-template) as a templating engine (But I'm planning to make my own)

# Motivation

My first exposure to full stack web frameworks was with [ruby on rails](http://rubyonrails.org/) and I fell in love with it. At the same time I was learning Prolog and came across a paper about [Creating web applications with Prolog](http://www.pathwayslms.com/swipltuts/html/index.html) and I couldn't help, but wonder if I could make a Rails-like framework for Prolog.

So why not Prolog on Rails?
I named my project first WebPl and trying to pronounce it as a word i changed it around a bit! :D

# About me

I'm by far not a Prolog expert and I don't even claim to be good at Prolog, but I'm willing to learn and put time into it so if you see some unhealthy code don't hesitate to contact me!

On the other hand, this is just a side-project for me so if you find any errors or have good new ideas for features or see that I haven't touched this for a while just drop me an e-mail!

# TODOs:

1. Move wobble.pl into an installable swipl module.
2. Make a uniform interface for GET and POST actions.(http_parameters for template)
3. DAL implementation first with SWI-Pl's odbc than for some other DBs too.
4. Implement own Template engine (probably ERB-like format).
5. Builders, Scaffolds and all that neat stuff!